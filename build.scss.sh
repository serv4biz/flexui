#!/bin/bash

# for bun js
# install bun js first
# bun install sass
# bun install postcss autoprefixer

echo "Begin compiling scss"
bun sass --style compressed ./assets/scss/style.scss ./assets/css/style.css

rm -rf ./assets/css/style.css.map
bun postcss ./assets/css/*.css --replace --map --use autoprefixer

cp -rf ./assets/scss/fontawesome/webfonts ./assets
cp -rf ./assets/scss/bootstrap-icons/fonts ./assets/css
echo "End of compile scss"


# for node js
# install node via package manager
# See this -> https://nodejs.org/en/download/package-manager/

# Requrire cli application
# sudo npm install -g sass
# sudo npm install -g postcss postcss-cli autoprefixer

# echo "Begin compiling scss"
# sass --style compressed ./assets/scss/style.scss ./assets/css/style.css
# npx postcss ./assets/css/*.css -r -m --use autoprefixer
# cp -rf ./assets/scss/fontawesome/webfonts ./assets
# cp -rf ./assets/scss/bootstrap-icons/fonts ./assets/css
# echo "End of compile scss"