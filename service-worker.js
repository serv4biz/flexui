// Listen for install event, set callback
self.addEventListener('install', function (event) { });

self.addEventListener('activate', function (event) { });

self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request).then(function (response) {
      return response || fetch(event.request);
    })
  );
});